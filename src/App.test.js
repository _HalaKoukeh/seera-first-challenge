import React from 'react';
import { render, cleanup, fireEvent, screen, waitFor } from '@testing-library/react';
import Items from './Items';
import Enzyme, { mount, shallow } from 'enzyme';

import App from './App'

afterEach(() => cleanup())



describe('Test General Render', () => {

  it('should display empty state if empty info was passed ', () => {
    render(<Items items={[]} totalNights={5} />);
    const itemsEl = screen.getByTestId('not-found')
    expect(itemsEl).toBeInTheDocument()
  })

  it('should display an item & its correct details', () => {
    const items = [
      {
        "name": "Kempinski Hotel Mall of the Emirates",
        "price": "200",
        "city": "dubai",
        "available_on": "2020-10-21"
      }
    ];

    // const { container } = render(<Items items={items} totalNights={5} />);
    const container = render(<Items items={items} totalNights={5} />);
    const _hotel = screen.getByTestId('hotel-item');
    expect(_hotel).toBeInTheDocument()
    expect(_hotel).toHaveTextContent(200 * 5)
    expect(_hotel).toHaveTextContent(items[0].name)
    expect(_hotel).toHaveTextContent(items[0].city)
    expect(_hotel).toHaveTextContent(items[0].available_on)

    expect(2 + 2).toEqual(4)
  })
})



describe('Test General Filter', () => {
  it('should display all filters', async () => {

    const container = render(<App />);

    await waitFor(() => {
      let searchButton = screen.getByText('Search');
      fireEvent.click(searchButton);
      // 
      const hotelsListing = screen.getAllByTestId('hotel-item');
      if (hotelsListing.length) {
        let searchButton = screen.getByText('Search');
        fireEvent.click(searchButton);
      }

    })

    // expect(screen.getByTestId("counter-text")).toHaveTextContent("0 results");
  })
})

