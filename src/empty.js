
function EmptyResults() {
    return (
        <div data-testid="not-found" className="not-found">
            <img className="not-found" src="/not-found.svg" alt="not-found" />
            No Hotels were found for selected options!
            <small className="block"><b>Try different Filters</b></small>
        </div>
    )
}
export default EmptyResults;
