import ReactPaginate from 'react-paginate';
import React, { useState, useEffect } from "react";

import EmptyResults from './empty';

function ItemDisplay(props) {
    return (<div className={props.item.city + ' item'} data-testid='hotel-item' >
        <p className="title">{props.item.name}</p>
        <p><b>Night Price</b>{props.item.price}</p>
        <p><b>City</b> {props.item.city}</p>
        <p className="date"><b>Available On</b>{props.item.available_on}</p>
        <p className="price"><b>Full Price</b>{(props.item.price) * props.totalNights}</p>

    </div>)
}

function Items(props) {
    const perPage = 5;
    const pageCount = Math.ceil(props.items.length / perPage);
    const [currentPage, setcurrentPage] = useState(0);

    const [currentItems, setcurrentItems] = useState(props.items)


    const handlePageClick = (e) => {
        setcurrentPage(e.selected);
    };

    useEffect(() => {
        setcurrentPage(0);
    }, [props.items])


    useEffect(() => {
        const _items = props.items.slice(currentPage * pageCount, perPage);
        setcurrentItems(_items)
    }, [currentPage, props.items, pageCount])

    return (

        (currentItems.length == 0) ? <EmptyResults /> :

            < div className="content" >
                {currentItems.map((item, index) => <ItemDisplay data-testid="lalalalala" key={`#${index}`} item={item} totalNights={props.totalNights} />)}
                < ReactPaginate
                    previousLabel={"prev"}
                    nextLabel={"next"}
                    breakLabel={"..."}
                    breakClassName={"break-me"}
                    pageCount={pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={perPage}
                    onPageChange={handlePageClick.bind(this)}
                    containerClassName={"pagination"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"} />
            </div >
    )
}

export default Items;
