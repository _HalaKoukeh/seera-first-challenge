import './App.css';
import React from "react";
import axios from 'axios';

import Items from './Items';
const url = 'http://www.mocky.io/v2/5eb8fcb12d0000d088357f2a';

function App() {

  const [fromDate, setFromDate] = React.useState('2020-10-22');
  const [toDate, setToDate] = React.useState('2020-10-29');

  const [hotels, setHotels] = React.useState([])
  const [availableHotels, setAvailableHotels] = React.useState([])

  const [pricerange, setPriceRange] = React.useState([0, 0])

  const [searchTerm, setSearchTerm] = React.useState('');
  const [searchResults, setSearchResults] = React.useState([]);

  const [priceFilter, setPriceFilter] = React.useState(0);
  const [sortBy, setSortBy] = React.useState("");


  const [totalNights, setTotalNights] = React.useState(0);

  const getAllHotels = () => {
    axios.get(url)
      .then(
        (resp) => {
          // eslint-disable-next-line 
          let hotels = JSON.parse(resp.data.replace(/\,(?!\s*?[\{\[\"\'\w])/g, ''));
          setHotels(hotels)
          console.log('alled in ')

        })
      .catch(error => {
        console.log(`Error: ${error}`)
        console.log('alled in ')

      })
  }

  React.useEffect(() => {

    getAllHotels();
  }, []);


  const handleFromDateChange = event => {
    if (toDate && event.target.value > toDate) {
      alert('You are selecting a later date')
      return;
    }

    else if (event.target.value > new Date())
      alert('Cannot select date in the past')

    else
      setFromDate(event.target.value);
  };


  const handleToDateChange = event => {
    if (fromDate && event.target.value < fromDate) {
      alert('You are selecting an earlier date')
      return;
    }

    else if (event.target.value > new Date())
      alert('Cannot select date in the past')

    else
      setToDate(event.target.value);
  };


  const handleDateFilter = () => {
    const _fromDate = new Date(fromDate).getTime();
    const _toDate = new Date(toDate).getTime();
    const duration = (_toDate - _fromDate) / (24 * 3600 * 1000);
    setTotalNights(duration)

    const results = hotels.filter(hotel => new Date(hotel.available_on).getTime() > _fromDate && new Date(hotel.available_on).getTime() <= _toDate)
    if (results.length) {
      let min = hotels[0].price, max = hotels[0].price;
      results.forEach(r => {
        if (r.price > max)
          max = r.price
        if (r.price < min)
          min = r.price
      })
      setPriceRange([min, max])
      setPriceFilter(max)
    }
    setAvailableHotels(results);

  };

  const handleSearchTermChange = event => {
    setSearchTerm(event.target.value);
  };

  const handlePriceFilterChange = event => {
    setPriceFilter(event.target.value)
  };

  const handleSortingChange = event => {
    setSortBy(event.target.value);
  };

  React.useEffect(() => {
    const results = availableHotels.filter(hotel =>
      hotel.name.toLowerCase().includes(searchTerm.toLowerCase()) && hotel.price <= priceFilter
    );
    if (sortBy === 'price')
      results.sort((a, b) => a.price > b.price ? 1 : -1)
    else if (sortBy === 'name')
      results.sort((a, b) => a.name > b.name ? 1 : -1)
    else
      results.sort((a, b) => a.price > b.price ? 1 : -1)
    setSearchResults(results);
  }, [searchTerm, priceFilter, sortBy, availableHotels]);


  return (
    <div className="App">
      <div className="filters">
        <div className="form-field">
          <label htmlFor="fromDate">
            From Date
            <input
              name="fromDate"
              type="date"
              placeholder="From: "
              value={fromDate}
              onChange={handleFromDateChange}
            />
          </label>
        </div>
        <div className="form-field">
          <label htmlFor="toDate">
            To Date
            <input
              name="toDate"
              type="date"
              placeholder="To: "
              value={toDate}
              onChange={handleToDateChange}
            />
          </label>
        </div>
        <button onClick={handleDateFilter}>Search</button>
      </div>
      <div className="section-header">
        <h1 >Hotels</h1>
        <span data-testid="counter-text">{searchResults.length ? searchResults.length + ' results' : '0 results '}</span>
      </div>

      <div className="listing">
        <div >
          <div className="form-field vertical-align-middle">
            <label htmlFor="searchTerm">
              Search For Hotel:
              <input
                type="text"
                placeholder="Search"
                value={searchTerm}
                onChange={handleSearchTermChange}
              />
            </label>
          </div>
          <div className="form-field vertical-align-middle">
            <label htmlFor="price">
              Prices: <small>{pricerange[0]} - {pricerange[1]} </small>
              <input
                type="range" id="price" name="price"
                min={pricerange[0]} max={pricerange[1]}
                placeholder="Price:"
                value={priceFilter}
                onChange={handlePriceFilterChange}
              />{priceFilter}
            </label>
          </div>
          <div className="form-field vertical-align-middle">
            <label htmlFor="sortBy">
              Sort By
              <select id="sortBy" name="sortBy" placeholder="Sort By:"
                value={sortBy}
                onChange={handleSortingChange}>
                <option value="price">By Price</option>
                <option value="name">By Name</option>
              </select>

            </label>
          </div>
        </div>
        <p>Total Nights {totalNights}</p>
        <Items items={searchResults} totalNights={totalNights} />
      </div>
    </div>)
}

export default App;
